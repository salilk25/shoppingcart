package com.licious.shoppingcart.controller;

import com.licious.shoppingcart.dto.InventoryDto;
import com.licious.shoppingcart.entity.Inventory;
import com.licious.shoppingcart.entity.Product;
import com.licious.shoppingcart.service.InventoryService;
import com.licious.shoppingcart.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Validated
@RequestMapping("inventory")
public class InventoryController {

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private ProductService productService;

    @PostMapping("/createInventory")
    public String createInventory(@Valid @RequestBody InventoryDto inventory){
    return inventoryService.createInventory(inventory);
    }

    @GetMapping("/viewInventory")
    @Cacheable(value = "inventories")
    public List<Inventory> viewInventory(@Valid @RequestParam String id){
        return inventoryService.viewInventory();
    }


    @GetMapping("/totalQuantity")
    @Cacheable(value = "totalQuantity")
    public int totalQuantity(@Valid @RequestParam String id){
        Product product = productService.viewProduct(id);
        return inventoryService.totalQuanity(product);
    }
}
