package com.licious.shoppingcart.controller;

import com.licious.shoppingcart.dto.CartDto;
import com.licious.shoppingcart.entity.Cart;
import com.licious.shoppingcart.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Validated
public class HomeController {
    @Autowired
    private CartService cartService;

    @GetMapping("/getProduct")
    @Cacheable(value = "carts")
    public List<Cart> findAllProduct() {
        return cartService.viewCart();
    }

    @PostMapping("/addNewProduct")
    public String addNewProduct(@Valid @RequestBody CartDto cartDto) {
        return cartService.addNewProduct(cartDto);
    }

    @PostMapping("/addProductExistingCart")
    @CachePut(value = "carts",key = "#id")
    public String addProductExistingCart(@Valid @RequestParam String id,@Valid @RequestParam int quantity) {
        return cartService.modifyExistingProductQuantity(id,quantity);
    }

    @PostMapping("/checkout")
    @CachePut(value = "carts",key = "#id")
    public void checkoutProduct(@Valid @RequestParam String id) {
        cartService.checkOut(id);

    }

    @DeleteMapping("/deleteCart")
    @CacheEvict(value = "carts", allEntries = true)
    public String deleteProduct(@Valid @RequestParam String id){
        cartService.deleteProduct(id);
        return "deleted";
    }



}
