package com.licious.shoppingcart.controller;

import com.licious.shoppingcart.dto.ProductDto;
import com.licious.shoppingcart.entity.Product;
import com.licious.shoppingcart.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@Validated
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/saveProduct")
    public String saveProduct(@Valid @RequestBody ProductDto product){
        return productService.addProduct(product);
    }

    @GetMapping("/products")
    @Cacheable(value = "products")
    public List<Product> findProduct(){
        return productService.viewProducts();
    }

    @PostMapping("/updateProduct")
    @CachePut(value = "products",key = "#id")
    public Product updateProduct(@Valid @RequestParam String id, @Valid @RequestBody ProductDto product){
        return productService.editProduct(id,product);
    }

    @DeleteMapping("/deleteProduct")
    @CacheEvict(value = "products", allEntries = true)
    public void deleteProduct(@Valid @RequestParam String id){
        productService.deleteProduct(id);


    }

}

