package com.licious.shoppingcart.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Document(collection = "product")
@Data
public class Product implements Serializable {
    @Id
    private String id;
    @NotNull(message = "name cannot be null or empty value")
    private String name;
    @NotNull(message = "price cannot be null")
    @Digits(integer = 8, message = "please enter the the numeric value", fraction = 3)
    private Integer price;
}
