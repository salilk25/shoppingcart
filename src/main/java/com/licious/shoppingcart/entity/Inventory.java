package com.licious.shoppingcart.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Document(collection = "inventory")
@Data
public class Inventory implements Serializable {
    private String id;
    @NotNull(message = "Product cannot be null")
    private Product product;
    @NotNull
    private int quantity;
}
