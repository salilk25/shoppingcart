package com.licious.shoppingcart.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Document(collection = "cart")
@Data
public class Cart implements Serializable {
    @Id
    private String id;
    @NotNull
    private Product product;
    @NotNull
    private int quantity;
    @NotNull
    private boolean checkoutStatus;
}
