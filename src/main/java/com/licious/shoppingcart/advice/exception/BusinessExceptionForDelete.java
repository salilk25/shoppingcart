package com.licious.shoppingcart.advice.exception;
import org.springframework.stereotype.Component;

@Component
public class BusinessExceptionForDelete extends RuntimeException{
    private String errorCode;
    private String errorMessage;

    public BusinessExceptionForDelete() {
    }

    public BusinessExceptionForDelete(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

