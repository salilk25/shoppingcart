package com.licious.shoppingcart.advice;


import com.licious.shoppingcart.advice.exception.BusinessExceptionForDelete;
import com.licious.shoppingcart.advice.exception.ExceptionResponse;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.Date;

@RestControllerAdvice
public class GlobalException extends Exception {

    @ExceptionHandler(ChangeSetPersister.NotFoundException.class)
    ResponseEntity<String> errorNotFound(ChangeSetPersister.NotFoundException e){
        return new ResponseEntity<>(e.getMessage()+"this is happening due to no records",HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BusinessExceptionForDelete.class)
    ResponseEntity<String> errorForDelete(BusinessExceptionForDelete e){
        return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(NoHandlerFoundException.class)
    public final ResponseEntity<ExceptionResponse> handleNotFoundException(NoHandlerFoundException ex, WebRequest request) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(),
                request.getDescription(false),HttpStatus.NOT_ACCEPTABLE.getReasonPhrase());
        return new ResponseEntity<ExceptionResponse>(exceptionResponse, HttpStatus.METHOD_NOT_ALLOWED);
    }
}
