package com.licious.shoppingcart.service;

import com.licious.shoppingcart.dto.CartDto;
import com.licious.shoppingcart.entity.Cart;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CartService {
String addNewProduct(CartDto cartDto);
String modifyExistingProductQuantity(String id,int quantity);
List<Cart> viewCart();
ResponseEntity<String> deleteProduct(String id);
String checkOut(String id);
}
