package com.licious.shoppingcart.service;

import com.licious.shoppingcart.advice.exception.BusinessExceptionForDelete;
import com.licious.shoppingcart.dto.CartDto;
import com.licious.shoppingcart.entity.Cart;
import com.licious.shoppingcart.entity.Inventory;
import com.licious.shoppingcart.mapper.Mapper;
import com.licious.shoppingcart.repository.CartRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;

@Service
public class CartServiceImpl implements CartService {

    Logger logger = LoggerFactory.getLogger(CartServiceImpl.class);

    @Autowired
    private CartRepo cartRepo;

    @Autowired
    private Mapper mapper;

    @Autowired
    private InventoryService inventoryService;

    @Override
    public String addNewProduct(CartDto cartDto) {
        Cart cart = mapper.dtoToEntity(cartDto);
        return cartRepo.save(cart).getId();
    }


    @Override
    public String modifyExistingProductQuantity(String id, int newQuantity) {
        logger.info("Updating to DB....");
        Optional<Cart> cartFromDb = cartRepo.findById(id);
        if (cartFromDb.isPresent()){
            Cart cart = cartFromDb.get();
            cart.setQuantity(newQuantity);
            cartRepo.save(cart);
            return "Your Product Quantity has been updated";
        }
        return null;
    }

    @Override
    public List<Cart> viewCart() {
        logger.info("Fetching from DB....");
        return cartRepo.findAll();
    }

    @Override
    public ResponseEntity<String> deleteProduct(String id) {
        try{
            cartRepo.deleteById(id);
            return new ResponseEntity<>(id, HttpStatus.OK);
        }catch (HttpClientErrorException.NotFound e){
            BusinessExceptionForDelete businessExceptionForDelete = new BusinessExceptionForDelete();
            businessExceptionForDelete.setErrorMessage(e.getMessage()+"this is from product service");
            throw businessExceptionForDelete;
        }
    }


    @Override
    public String checkOut(String id) {
        logger.info("Updating to DB....");
        Optional<Cart>cart1 = cartRepo.findById(id);
        if (cart1.isPresent()){
            Cart cart = cart1.get();
            if(cart.isCheckoutStatus()){
                return "product already checked out"+id;
            }
            cart.setCheckoutStatus(true);
            Inventory inventory = new Inventory();
            inventory.setQuantity(cart.getQuantity()*(-1));
            inventory.setProduct(cart.getProduct());
            inventoryService.createInventory(mapper.entityToDtoInventory(inventory));
            cartRepo.save(cart);
            return "checked out the cart with id"+id;}
        return null;
    }

}
