package com.licious.shoppingcart.service;

import com.licious.shoppingcart.dto.InventoryDto;
import com.licious.shoppingcart.entity.Inventory;
import com.licious.shoppingcart.entity.Product;

import java.util.List;


public interface InventoryService {
    String createInventory(InventoryDto inventory);

    List<Inventory> viewInventory();
    int totalQuanity(Product product);
}
