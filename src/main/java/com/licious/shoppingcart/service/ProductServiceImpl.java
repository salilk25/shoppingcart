package com.licious.shoppingcart.service;

import com.licious.shoppingcart.advice.exception.BusinessExceptionForDelete;
import com.licious.shoppingcart.dto.ProductDto;
import com.licious.shoppingcart.entity.Product;
import com.licious.shoppingcart.mapper.Mapper;
import com.licious.shoppingcart.repository.ProductRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepo repo;

    Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    private Mapper mapper;
    @Override
    public String addProduct(ProductDto productDto) {
        Product product = mapper.dtoToEntityProduct(productDto);
        return repo.save(product).getId();
    }

    @Override
    public Product editProduct(String id, ProductDto productDto) {
        logger.info("Updating to DB....");
        Optional<Product> productOptional = repo.findById(id);
        if(productOptional.isPresent()){
            Product productFromDb = productOptional.get();
            productFromDb.setPrice(productDto.getPrice());
            productFromDb.setName(productDto.getName());
            productFromDb.setPrice(productDto.getPrice());
            return repo.save(productFromDb);
        }
      return null;
    }

    @Override
    public Product viewProduct(String id) {
        logger.info("Fetching from DB....");
        if (repo.findById(id).isPresent()){
        return repo.findById(id).get();}
        return null;
    }
    @Override
    public List<Product> viewProducts() {

        logger.info("Fetching from DB....");
        return repo.findAll();
    }

    @Override
    public ResponseEntity<String> deleteProduct(String id) {
        try{
            repo.deleteById(id);
            return new ResponseEntity<>(id, HttpStatus.OK);
        }catch (HttpClientErrorException.NotFound e){
            BusinessExceptionForDelete businessExceptionForDelete = new BusinessExceptionForDelete();
            businessExceptionForDelete.setErrorMessage(e.getMessage()+"this is from product service");
            throw businessExceptionForDelete;
        }
    }
}
