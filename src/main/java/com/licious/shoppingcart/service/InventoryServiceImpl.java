package com.licious.shoppingcart.service;

import com.licious.shoppingcart.dto.InventoryDto;
import com.licious.shoppingcart.entity.Inventory;
import com.licious.shoppingcart.entity.Product;
import com.licious.shoppingcart.mapper.Mapper;
import com.licious.shoppingcart.repository.InventoryRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class InventoryServiceImpl implements InventoryService{

    @Autowired
    private InventoryRepo inventoryRepo;

    Logger logger = LoggerFactory.getLogger(InventoryServiceImpl.class);

    @Autowired
    private Mapper mapper;

    @Override
    public String createInventory(InventoryDto inventoryDto) {
        Inventory inventory = mapper.dtoToEntityInventory(inventoryDto);
        return inventoryRepo.save(inventory).getId();

    }

    @Override
    public List<Inventory> viewInventory() {
        logger.info("Fetching from DB....");
      return inventoryRepo.findAll();
    }


    @Override
    public int totalQuanity(Product product) {

        int total =0;// add optional
       List<Inventory> inventories= inventoryRepo.findAllByProduct(product);
        for(int i =0;i< inventories.size();i++){
            Inventory inventory = inventories.get(i);
            total = inventory.getQuantity()+ total;
        }
        return total;
    }
}
