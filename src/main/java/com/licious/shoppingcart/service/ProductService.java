package com.licious.shoppingcart.service;

import com.licious.shoppingcart.dto.ProductDto;
import com.licious.shoppingcart.entity.Product;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProductService {
    String addProduct(ProductDto product);
    Product editProduct(String id,ProductDto productDto);
    Product viewProduct(String id);

    List<Product> viewProducts();

    ResponseEntity<String> deleteProduct(String id);
}
