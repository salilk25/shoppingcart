package com.licious.shoppingcart.dto;

import com.licious.shoppingcart.entity.Product;
import lombok.Data;

@Data
public class InventoryDto {
    private Product product;
    private int quantity;
}
