package com.licious.shoppingcart.dto;

import com.licious.shoppingcart.entity.Product;
import lombok.Data;


@Data
public class CartDto {
    private Product product;
    private int quantity;
    private boolean checkoutStatus;
}
