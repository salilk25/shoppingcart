package com.licious.shoppingcart.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ProductDto {
    @NotNull(message = "name cannot be null or empty value")
    private String name;
    @NotNull(message = "price cannot be null or empty value")
    private Integer price;
}
