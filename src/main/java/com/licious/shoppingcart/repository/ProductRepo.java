package com.licious.shoppingcart.repository;

import com.licious.shoppingcart.entity.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepo extends MongoRepository<Product,Integer> {
    Optional<Product> findById(String id);
     void deleteById(String id);
}
