package com.licious.shoppingcart.repository;

import com.licious.shoppingcart.entity.Cart;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CartRepo extends MongoRepository<Cart,Integer> {
    Optional<Cart> findById(String id);

    void deleteById(String id);
}
