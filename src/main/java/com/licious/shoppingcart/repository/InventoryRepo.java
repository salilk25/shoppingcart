package com.licious.shoppingcart.repository;

import com.licious.shoppingcart.entity.Inventory;
import com.licious.shoppingcart.entity.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryRepo extends MongoRepository<Inventory,Integer> {
    List<Inventory> findAllByProduct(Product product);
}
