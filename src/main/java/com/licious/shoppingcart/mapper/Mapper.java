package com.licious.shoppingcart.mapper;

import com.licious.shoppingcart.dto.CartDto;
import com.licious.shoppingcart.dto.InventoryDto;
import com.licious.shoppingcart.dto.ProductDto;
import com.licious.shoppingcart.entity.Cart;
import com.licious.shoppingcart.entity.Inventory;
import com.licious.shoppingcart.entity.Product;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class Mapper {
    @Autowired
    private ModelMapper modelMapper;

    public CartDto entityToDto(Cart cart) {
        return modelMapper.map(cart,CartDto.class);

    }

    public InventoryDto entityToDtoInventory(Inventory inventory) {
        return modelMapper.map(inventory,InventoryDto.class);
    }


    public Cart dtoToEntity(CartDto cartDto) {
        return modelMapper.map(cartDto,Cart.class);
    }

    public Product dtoToEntityProduct(ProductDto productDto) {
        return modelMapper.map(productDto, Product.class);
    }
    public Inventory dtoToEntityInventory(InventoryDto inventoryDto) {
        return modelMapper.map(inventoryDto, Inventory.class);
    }

    public List<CartDto> entityToDto(List<Cart> carts) {
        return	carts.stream().map(this::entityToDto).collect(Collectors.toList());
    }

}
