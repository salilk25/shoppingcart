package com.licious.shoppingcart.service;

import com.licious.shoppingcart.dto.CartDto;
import com.licious.shoppingcart.dto.InventoryDto;
import com.licious.shoppingcart.dto.ProductDto;
import com.licious.shoppingcart.entity.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ServiceTest {
    @Autowired
    private ProductService productService;

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private CartService cartService;

    @Test
    void saveProduct(){
        ProductDto productDto = new ProductDto();
        productDto.setPrice(232);
        productDto.setName("lunch");
        productService.addProduct(productDto);
        Assertions.assertNotNull(productService.viewProducts());
    }

    @Test
    void saveInventory(){
        ProductDto productDto = new ProductDto();
        productDto.setPrice(232);
        productDto.setName("lunch");
        productService.addProduct(productDto);
        Product product = productService.viewProducts().get(0);
        InventoryDto inventoryDto = new InventoryDto();
        inventoryDto.setProduct(product);
        inventoryDto.setQuantity(5);
        Assertions.assertNotNull(inventoryService.viewInventory());
    }
    @Test
    void saveCart(){
        ProductDto productDto = new ProductDto();
        productDto.setPrice(232);
        productDto.setName("lunch");
        productService.addProduct(productDto);
        Product product = productService.viewProducts().get(0);
        InventoryDto inventoryDto = new InventoryDto();
        inventoryDto.setProduct(product);
        inventoryDto.setQuantity(5);
        CartDto cartDto = new CartDto();
        cartDto.setProduct(product);
        cartDto.setQuantity(2);
        cartService.addNewProduct(cartDto);
        Assertions.assertNotNull(cartService.viewCart());
    }
}
