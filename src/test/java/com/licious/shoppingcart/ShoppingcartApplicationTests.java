package com.licious.shoppingcart;

import com.licious.shoppingcart.dto.ProductDto;
import com.licious.shoppingcart.entity.Product;
import com.licious.shoppingcart.service.ProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ShoppingcartApplicationTests {

	@Autowired
	private ProductService productService;
	@Test
	void contextLoads() {
		ProductDto productDto = new ProductDto();
		productDto.setPrice(232);
		productDto.setName("lunch");
		productService.addProduct(productDto);
		Assertions.assertNotNull(productService.viewProducts());
	}

}
